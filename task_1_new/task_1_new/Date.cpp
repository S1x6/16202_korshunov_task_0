#include "Date.h"
#include "DateInterval.h"

namespace calendar {

	const std::string Date::months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	Date::Date()
	{
		time_t t = time(0);
		std::tm *time = gmtime(&t);
		second = time->tm_sec;
		minute = time->tm_min;
		hour = time->tm_hour;
		month_day = time->tm_mday;
		month = (Month) (time->tm_mon+1);
		year = time->tm_year+1900;
		isLeapyear = ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) ? true : false;
	}

	Date::Date(uint year, Month month, uint day, uint hour, uint minute, uint second)
	{
		minute += second / 60;
		second = second % 60;
		hour += minute / 60;
		minute = minute % 60;
		day += hour / 24;
		hour = hour % 24;
		this->minute = minute;
		this->second = second;
		this->hour = hour;
		this->month = month;
		this->month_day = day;
		this->year = year;
		bool needsUpdate = true;
		while (needsUpdate) {
			needsUpdate = addMonthFromDays(&this->month, &this->month_day, &this->year);
		}
		if (this->year == 9999 && this->month == Dec && this->month_day > 31) {
			this->month_day = 31;
			this->hour = 23;
			this->minute = 59;
			this->second = 59;
		}
		isLeapyear = ((this->year % 4 == 0 && this->year % 100 != 0) || this->year % 400 == 0) ? true : false;
	}

	Date::Date(uint year, Month m, uint day)
	{
		hour = 0;
		minute = 0;
		second = 0;
		this->month = m;
		this->month_day = day;
		this->year = year;
		bool needsUpdate = true;
		while (needsUpdate) {
			needsUpdate = addMonthFromDays(&this->month, &this->month_day, &this->year);
		}
		if (this->year == 9999 && this->month == Dec && this->month_day > 31) {
			this->month_day = 31;
			this->hour = 23;
			this->minute = 59;
			this->second = 59;
		}
		isLeapyear = ((this->year % 4 == 0 && this->year % 100 != 0) || this->year % 400 == 0) ? true : false;
	}

	Date::Date(uint hrs, uint mnts, uint seconds)
	{
		time_t t = time(0);
		std::tm *time = gmtime(&t);
		month_day = time->tm_mday;
		month = (Month)(time->tm_mon + 1);
		year = time->tm_year + 1900;

		minute += seconds / 60;
		second = seconds % 60;
		hour += mnts / 60;
		minute = mnts % 60;
		month_day += hrs / 24;
		hour = hrs % 24;
		
		bool needsUpdate = true;
		while (needsUpdate) {
			needsUpdate = addMonthFromDays(&this->month, &this->month_day, &this->year);
		}
		if (this->year == 9999 && this->month == Dec && this->month_day > 31) {
			this->month_day = 31;
			this->hour = 23;
			this->minute = 59;
			this->second = 59;
		}

		isLeapyear = ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) ? true : false;
	}

	bool Date::addMonthFromDays(Month* m, int* d, int* y) 
	{
		int day_per_month;
		switch (*m)
		{
		case calendar::Date::Jan:
		case calendar::Date::Mar:
		case calendar::Date::May:
		case calendar::Date::Jul:
		case calendar::Date::Aug:
		case calendar::Date::Oct:
		case calendar::Date::Dec:
			day_per_month = 31;
			break;
		case calendar::Date::Apr:
		case calendar::Date::Jun:
		case calendar::Date::Sep:
		case calendar::Date::Nov:
			day_per_month = 30;
			break;
		case calendar::Date::Feb:
			if ((*y % 4 == 0 && *y % 100 != 0) || *y % 400 == 0) {
				day_per_month = 29;
			} else {
				day_per_month = 28;
			}
			break;
		default:
			break;
		}
		
		if (*d > 0 && day_per_month < *d) {
			if (*m < 12) {
				*d -= day_per_month;
				*m = (Month) (*m + 1);
				return 1;
			}
			else {
				if (*y < 9999) {
					(*y)++;
					*d -= day_per_month;
					*m = Jan;
					return 1;
				}
				else {
					return 0;
				}
			}
		}
		else {
			return 0;
		}
	}

	bool Date::substractMonthFromDays(Month* m, int* d, int* y)
	{
		if (*y == 1 && *m == Jan && *d < 1) {
			return 0;
		}
		int day_per_month;
		switch (((*m + 10) % 12)+1) // +10 equals -1
		{
		case calendar::Date::Jan:
		case calendar::Date::Mar:
		case calendar::Date::May:
		case calendar::Date::Jul:
		case calendar::Date::Aug:
		case calendar::Date::Oct:
		case calendar::Date::Dec:
			day_per_month = 31;
			break;
		case calendar::Date::Apr:
		case calendar::Date::Jun:
		case calendar::Date::Sep:
		case calendar::Date::Nov:
			day_per_month = 30;
			break;
		case calendar::Date::Feb:
			if ((*y % 4 == 0 && *y % 100 != 0) || *y % 400 == 0) {
				day_per_month = 29;
			}
			else {
				day_per_month = 28;
			}
			break;
		default:
			break;
		}

		if (*d <= 0) {
			if (*m > 1) {
				*d += day_per_month;
				*m = (Month)(((*m + 10) % 12) + 1);
				return 1;
			}
			else {
				if (*y > 1) {
					(*y)--;
					*d += day_per_month;
					*m = Dec;
					return 1;
				}
				else {
					return 0;
				}
			}
		}
		else {
			return 0;
		}
	}

	std::string Date::toString() const
	{
		using namespace std;
		string d = month_day < 10 ? "0" + to_string(month_day) : to_string(month_day);
		string h = hour < 10 ? "0" + to_string(hour) : to_string(hour);
		string m = minute < 10 ? "0" + to_string(minute) : to_string(minute);
		string s = second < 10 ? "0" + to_string(second) : to_string(second);
		string y = to_string(year);
		return y + "-" + months[month - 1] + "-" + d + " " + h + "::" + m + "::" + s;;
	}

	Date::Date(const Date &date)
	{
		second = date.second;
		minute = date.minute;
		hour = date.hour;
		month_day = date.month_day;
		month = date.month;
		year = date.year;
	}

	Date::~Date()
	{
	}

	Date::uint Date::getYear() const
	{
		return year;
	}

	Date::Month Date::getMonth() const
	{
		return month;
	}

	Date::uint Date::getDay() const
	{
		return month_day;
	}

	Date::uint Date::getHour() const
	{
		return hour;
	}

	Date::uint Date::getMinute() const
	{
		return minute;
	}

	Date::uint Date::getSecond() const
	{
		return second;
	}

	DateInterval Date::getInterval(const Date & another) const
	{
		return DateInterval(*this, another);
	}

	Date calendar::Date::addYears(int dYears)
	{
		const int y = this->year + dYears;
		if (y > 9999) {
			return Date(9999, Dec, 31, 23, 59, 59);
		}
		else if (y < 1) {
			return Date(1, Jan, 1, 0, 0, 0);
		}
		else {
			return Date(y, this->month, this->month_day, this->hour, this->minute, this->second);
		}
	}

	Date calendar::Date::addMonths(int dMonths)
	{
		int m = (this->year-1) * 12 + this->month-1 + dMonths;
		if (m < 1) {
			return Date(1, Jan, 1, 0, 0, 0);
		}
		else if (m > 9999 * 12 + 12) {
			return Date(9999, Dec, 31, 23, 59, 59);
		}
		else
		{
			return Date(m / 12+1, (Month)(((m) % 12)+1), this->month_day, this->hour, this->minute, this->second); 
		}
	}

	Date Date::addDays(int dDays)
	{
		if (dDays == 0) {
			return Date(this->year, this->month, this->month_day, this->hour, this->minute, this->second);
		}
		int d = this->month_day + dDays;
		Month m = this->month;
		int y = this->year;
		bool needsUpdate = true;
		if (d < 1) {
			while (needsUpdate) {
				needsUpdate = substractMonthFromDays(&m, &d, &y);
			}
		}
		else {
			while (needsUpdate) {
				needsUpdate = addMonthFromDays(&m, &d, &y);
			}
		}
		if (d < 1) {
			return Date(1, Jan, 1);
		}
		else if (d > 31) {
			return Date(9999, Dec, 31, 23, 59, 59);
		}
		return Date(y, m, d, this->hour, this->minute, this->second);
	}

	Date Date::addHours(int dHours)
	{
		int dDay;
		if (this->hour + dHours < 0) {
			dDay = (this->hour + dHours - 24) / 24;
		}
		else {
			dDay = (this->hour + dHours) / 24;
		}
		Date date = Date(*this).addDays(dDay);
		if (checkIfMaxDate(date) || checkIfMinDate(date)) {
			return date;
		}
		int new_hour = (this->hour + dHours) % 24;
		if (new_hour < 0) {
			new_hour += 24;
		}
		date.hour = new_hour;
		return date;
	}

	Date Date::addMinutes(int dMinutes)
	{
		int m = this->minute + dMinutes;
		int dHour;
		if (m < 0) {
			dHour = (m - 60) / 60;
		}
		else {
			dHour = (m) / 60;
		}
		Date date = Date(*this).addHours(dHour);
		if (checkIfMaxDate(date) || checkIfMinDate(date)) {
			return date;
		}
		int new_minutes = (this->minute + dMinutes) % 60;
		if (new_minutes < 0) {
			new_minutes += 60;
		}
		date.minute = new_minutes;
		return date;
	}

	Date Date::addSeconds(int dSec)
	{
		int s = this->second + dSec;
		int dMin;
		if (s < 0) {
			dMin = (s - 60) / 60;
		}
		else {
			dMin = (s / 60);
		}
		Date date = Date(*this).addMinutes(dMin);
		if (checkIfMaxDate(date) || checkIfMinDate(date)) {
			return date;
		}
		int new_seconds = (this->second + dSec) % 60;
		if (new_seconds < 0) {
			new_seconds += 60;
		}
		date.second = new_seconds;
		return date;
	}

	bool Date::checkIfMaxDate(Date &d)
	{
		if (d.year == 9999 && d.month == Dec && d.month_day == 31 && d.minute == 59 && d.hour == 23 && d.second == 59) {
			return true;
		}
		return false;
	}

	bool Date::checkIfMinDate(Date &d)
	{
		if (d.year == 1 && d.month == Jan && d.month_day == 1 && d.minute == 0 && d.hour == 0 && d.second == 0) {
			return true;
		}
		return false;
	}

	const Date& Date::operator=(const Date &new_d)
	{
		this->year = new_d.year;
		this->month = new_d.month;
		this->month_day = new_d.month_day;
		this->hour = new_d.hour;
		this->minute = new_d.minute;
		this->second = new_d.second;
		this->isLeapyear = new_d.isLeapyear;
		return *this;
	}

	Date Date::addInterval(const DateInterval & di) const
	{
		return Date(*this).addSeconds(di.getSecondDifference()).addMinutes(di.getMinuteDifference()).addHours(di.getHourDifference()).
			addDays(di.getDayDifference()).addMonths(di.getMonthDifference()).addYears(di.getYearDifference());
	}

	bool operator<(const Date& a, const Date & date)
	{
		int y = a.year < date.year ? 1 : a.year == date.year ? 0 : -1;
		if (y == 1) return true; else if (y == -1) return false;
		int m = a.month < date.month ? 1 : a.month == date.month ? 0 : -1;
		if (m == 1) return true;  else if (m == -1) return false;
		int d = a.month_day < date.month_day ? 1 : a.month_day == date.month_day ? 0 : -1;
		if (d == 1) return true;  else if (d == -1) return false;
		int h = a.hour < date.hour ? 1 : a.hour == date.hour ? 0 : -1;
		if (h == 1) return true;  else if (h == -1) return false;
		int mi = a.minute < date.minute ? 1 : a.minute == date.minute ? 0 : -1;
		if (mi == 1) return true; else if (mi == -1) return false;
		int s = a.second < date.second ? 1 : a.second == date.second ? 0 : -1;
		if (s == 1)
			return true;
		else 
			return false;
	}

	Date::uint calendar::Date::dayPerMonth(Month const & m, bool isLeapYear)
	{
		switch (((m + 10) % 12) + 1) // +10 equals -1
		{
		case calendar::Date::Jan:
		case calendar::Date::Mar:
		case calendar::Date::May:
		case calendar::Date::Jul:
		case calendar::Date::Aug:
		case calendar::Date::Oct:
		case calendar::Date::Dec:
			return 31;
			break;
		case calendar::Date::Apr:
		case calendar::Date::Jun:
		case calendar::Date::Sep:
		case calendar::Date::Nov:
			return 30;
			break;
		case calendar::Date::Feb:
			if (isLeapYear) {
				return 29;
			}
			else {
				return 28;
			}
			break;
		}
		return 0;
	}

	bool calendar::Date::getIsLeapYear() const
	{
		return isLeapyear;
	}

	std::string Date::formatDate(std::string format)
	{
		std::string res = "";
		int size = format.size();
		for (int i = 0; i < size; ++i) {
			switch (format[i]) {
			case 'Y':
				if (i + 3 < size && format[i + 1] == 'Y' && format[i + 2] == 'Y' && format[i + 3] == 'Y' && (i + 4 < size && format[i + 4] != 'Y' || i + 4 == size)) {
					res += std::to_string(year);
					i += 3;
				}
				else if (i + 4 < size && format[i + 4] == 'Y') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			case 'M':
				if (i + 2 < size && format[i + 1] == 'M' && format[i + 2] == 'M' && (i + 3 < size && format[i + 3] != 'M' || i + 3 == size)) {
					res += months[month - 1];
					i += 2;
				}
				else if (i + 1 < size && format[i + 1] == 'M' && (i + 2 < size && format[i + 2] != 'M' || i + 2 == size)) {
					res += month < 10 ? "0" + std::to_string(month) : std::to_string(month);
					++i;
				}
				else if (i + 3 < size && format[i + 3] == 'M') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			case 'D':
				if (i + 1 < size && format[i + 1] == 'D' && (i + 2 < size && format[i + 2] != 'D' || i + 2 == size)) {
					res += month_day < 10 ? "0" + std::to_string(month_day) : std::to_string(month_day);
					++i;
				}
				else if (i + 2 < size && format[i + 2] == 'D') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			case 'h':
				if (i + 1 < size && format[i + 1] == 'h' && (i + 2 < size && format[i + 2] != 'h' || i + 2 == size)) {
					res += hour < 10 ? "0" + std::to_string(hour) : std::to_string(hour);
					++i;
				}
				else if (i + 2 < size && format[i + 2] == 'h') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			case 'm':
				if (i + 1 < size && format[i + 1] == 'm' && (i + 2 < size && format[i + 2] != 'm' || i + 2 == size)) {
					res += minute < 10 ? "0" + std::to_string(minute) : std::to_string(minute);
					++i;
				}
				else if (i + 2 < size && format[i + 2] == 'm') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			case 's':
				if (i + 1 < size && format[i + 1] == 's' && (i + 2 < size && format[i + 2] != 's' || i + 2 == size)) {
					res += second < 10 ? "0" + std::to_string(second) : std::to_string(second);
					++i;
				}
				else if (i + 2 < size && format[i + 2] == 's') {
					return "Invalid date format";
				}
				else {
					res += format[i];
				}
				break;
			default:
				res += format[i];
				break;
			}
		}
		return res;
	}
}