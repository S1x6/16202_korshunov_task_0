#include "DateInterval.h"
#include "Date.h"

namespace calendar {
	DateInterval::DateInterval()
		:year(0),
		month(0),
		day(0),
		hour(0),
		minute(0),
		second(0)
	{
	}

	DateInterval::DateInterval(const calendar::Date & a, const calendar::Date & b)
	{
		int dYear = b.getYear() - a.getYear(),
			dDay = b.getDay() - a.getDay(),
			dHour = b.getHour() - a.getHour(),
			dMinute = b.getMinute() - a.getMinute(),
			dSecond = b.getSecond() - a.getSecond(),
			dMonth = b.getMonth() - a.getMonth();
		if (b < a) {
			if (dSecond > 0) {
				dSecond -= 60;
				dMinute++;
			}
			if (dMinute > 0) {
				dMinute -= 60;
				dHour++;
			}
			if (dHour > 0) {
				dHour -= 24;
				dDay++;
			}
			if (dDay > 0) {
				dDay -= calendar::Date::dayPerMonth(b.getMonth(), b.getIsLeapYear());
				dMonth++;
			}
			if (dMonth > 0) {
				dMonth -= 12;
				dYear++;
			}
		}
		else {
			if (dSecond < 0) {
				dSecond += 60;
				dMinute--;
			}
			if (dMinute < 0) {
				dMinute += 60;
				dHour--;
			}
			if (dHour < 0) {
				dHour += 24;
				dDay--;
			}
			if (dDay < 0) {
				dDay += calendar::Date::dayPerMonth(b.getMonth(), b.getIsLeapYear());
				dMonth--;
			}
			if (dMonth < 0) {
				dMonth += 12;
				dYear--;
			}
		}
		year = dYear;
		month = dMonth;
		day = dDay;
		hour = dHour;
		minute = dMinute;
		second = dSecond;

	}

	DateInterval::DateInterval(const DateInterval & another)
	{
		year = another.year;
		month = another.month;
		day = another.day;
		hour = another.hour;
		minute = another.minute;
		second = another.second;
	}

	void DateInterval::print()
	{
		std::cout << year << "." << month << "." << day << " " << hour << ":" << minute << ":" << second << std::endl;
	}

	DateInterval::~DateInterval()
	{
		
	}

	int DateInterval::getYearDifference() const
	{
		return year;
	}

	int DateInterval::getMonthDifference() const
	{
		return month;
	}

	int DateInterval::getDayDifference()const
	{
		return day;
	}

	int DateInterval::getHourDifference()const
	{
		return hour;
	}

	int DateInterval::getMinuteDifference()const
	{
		return minute;
	}

	int DateInterval::getSecondDifference()const
	{
		return second;
	}

	const DateInterval & DateInterval::operator=(const DateInterval & another)
	{

		this->year = another.year;
		this->month = another.month;
		this->day = another.day;
		this->hour = another.hour;
		this->minute = another.minute;
		this->second = another.second;
		return *(this);
	}

}