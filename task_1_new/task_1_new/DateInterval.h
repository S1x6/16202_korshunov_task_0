#pragma once
#include <string>

namespace calendar {

	class Date;
	class DateInterval
	{
	public:
		DateInterval();
		DateInterval(const Date &a, const Date &b);
		DateInterval(const DateInterval &);
		~DateInterval();
		int getYearDifference()const;
		int getMonthDifference()const;
		int getDayDifference()const;
		int getHourDifference()const;
		int getMinuteDifference()const;
		int getSecondDifference()const;
		const DateInterval& operator=(const DateInterval&);
		void print();
	private:
		int year,
			month,
			day,
			hour,
			minute,
			second;
	};
}