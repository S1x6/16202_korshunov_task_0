#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <time.h>
#include <iostream>
#include <string>


namespace calendar {
	class DateInterval;
	class Date
	{
		typedef unsigned int uint;
	public:
		static const std::string months[12];
		enum Month {
			Jan = 1,
			Feb,
			Mar,
			Apr,
			May,
			Jun,
			Jul,
			Aug,
			Sep,
			Oct,
			Nov,
			Dec
		};
		Date();
		Date(uint year, Month month, uint day, uint hour, uint minute, uint second);
		Date(uint year, Month m, uint day);
		Date(uint hrs, uint mnts, uint seconds);
		Date(const Date&);
		~Date();
		static uint dayPerMonth(Month const & m, bool isLeapYear);
		Date addYears(int dYears);
		Date addMonths(int dMonths);
		Date addDays(int);
		Date addHours(int);
		Date addMinutes(int);
		Date addSeconds(int);
		uint getYear() const;
		Month getMonth() const;
		uint getDay() const;
		uint getHour() const;
		uint getMinute() const;
		uint getSecond() const;
		DateInterval getInterval(const Date &another) const;
		Date addInterval(const DateInterval &di) const;
		std::string toString() const;
		bool getIsLeapYear() const;
		const Date& operator=(const Date&);
		std::string formatDate(std::string format);
		friend bool operator<(const Date&, const Date&);

	private:
		int year;
		Month month;
		int month_day,
			hour,
			minute,
			second;
		bool isLeapyear;
		bool addMonthFromDays(Month*, int*, int*);
		bool substractMonthFromDays(Month* m, int* d, int* y);
		bool checkIfMaxDate(Date &d);
		bool checkIfMinDate(Date &d);
	};
}
