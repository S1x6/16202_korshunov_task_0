#define _CRT_SECURE_NO_WARNINGS
#include "Date.h"
#include "DateInterval.h"
#include "../../gtest/src/gtest-all.cc"

int main()
{
	using namespace calendar;
	Date d1 = Date(); // current date
	Date d2 = Date(2007, Date::Mar, 2, 15, 5, 0);

	// check constructor work
	assert(d2.getDay() == 2);
	assert(d2.getMonth() == Date::Mar);
	assert(d2.getHour() == 15);
	assert(d2.getMinute() == 5);
	assert(d2.getSecond() == 0);
	assert(d2.getYear() == 2007);
	assert(d2 < d1);

	// check 'addXXX' methods
	assert(d2.addYears(1).getYear() == d2.getYear() + 1);
	assert(d2.addDays(-2).getDay() == 28);
	assert(d2.addMonths(2).getMonth() == Date::May);
	assert(d2.addHours(24).getHour() == d2.getHour());
	assert(d2.addSeconds(-5).getSecond() == 55);
	assert(d2.addMinutes(61).getMinute() == 6);

	int y = d2.getYear();
	d2.addYears(111);
	assert(d2.getYear() == y);
	assert(d2.addYears(10000).getYear() == 9999);
	assert(d2.addYears(-100000).getYear() == 1);

	DateInterval di = DateInterval(d1, d2); // take interval by constructor
	std::cout << d2.toString() << std::endl;
	std::cout << d1.addInterval(di).toString() << std::endl;
	di.print();
	DateInterval di3 = d1.getInterval(d2);  // take interval from date
	std::cout << d1.addInterval(di3).toString() << std::endl;

	// checking 'formatDate' method
	assert(d2.formatDate("YYYY") == "2007");
	assert(d2.formatDate("YYYYaa") == "2007aa");
	assert(d2.formatDate("af YYYY") == "af 2007");
	assert(d2.formatDate("YYYYY") == "Invalid date format");
	assert(d2.formatDate("YYY") == "YYY");
	assert(d2.formatDate("YYY3") == "YYY3");
	assert(d2.formatDate("dYYY") == "dYYY");

	assert(d2.formatDate("DD") == "02");
	assert(d2.formatDate("DDaa") == "02aa");
	assert(d2.formatDate("af DD") == "af 02");
	assert(d2.formatDate("DDD") == "Invalid date format");
	assert(d2.formatDate("D") == "D");

	assert(d2.formatDate("MMM") == "Mar");
	assert(d2.formatDate("MMMaa") == "Maraa");
	assert(d2.formatDate("af MMM") == "af Mar");
	assert(d2.formatDate("MMMM") == "Invalid date format");
	assert(d2.formatDate("M") == "M");

	assert(d2.formatDate("MM") == "03");
	assert(d2.formatDate("MMaa") == "03aa");
	assert(d2.formatDate("af MM") == "af 03");

	assert(d2.formatDate("hh") == "15");
	assert(d2.formatDate("hhaa") == "15aa");
	assert(d2.formatDate("af hh") == "af 15");
	assert(d2.formatDate("hhh") == "Invalid date format");
	assert(d2.formatDate("h") == "h");

	assert(d2.formatDate("mm") == "05");
	assert(d2.formatDate("mmaa") == "05aa");
	assert(d2.formatDate("af mm") == "af 05");
	assert(d2.formatDate("mmm") == "Invalid date format");
	assert(d2.formatDate("m") == "m");

	assert(d2.formatDate("ss") == "00");
	assert(d2.formatDate("ssaa") == "00aa");
	assert(d2.formatDate("af ss") == "af 00");
	assert(d2.formatDate("sss") == "Invalid date format");
	assert(d2.formatDate("s") == "s");

	std::string dateString = d2.formatDate("This Date object represents the YYYY.MM.DD which is DD of MMM, YYYY year. The time is hh:mm:ss.");
	std::cout << dateString << std::endl;
	assert(dateString == "This Date object represents the 2007.03.02 which is 02 of Mar, 2007 year. The time is 15:05:00.");
	getchar();
	return 0;
}