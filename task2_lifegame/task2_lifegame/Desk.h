#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Cell.h"

class Desk
{
public:
	Desk();
	~Desk();
	void draw();
	std::vector< std::vector<Cell> > cellsVector;
};

