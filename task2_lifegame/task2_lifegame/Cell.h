#pragma once
#include <vector>
#include <iostream>

class Cell
{
public:
	Cell();
	~Cell();
	bool isAlive;
	void setNeighbours(std::vector<Cell*> neighbours);
	void checkState(); // calculates next state
	void applyState(); // applies next state
private:
	bool nextState;
	std::vector<Cell*> neighbours;
};

