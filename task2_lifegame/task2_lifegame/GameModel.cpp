#include "GameModel.h"
#include <fstream>
#include "gtest\src\gtest-all.cc"

enum CommandType {
	error,
	reset,
	set,
	clear,
	step,
	back,
	save,
	load
};

GameModel::GameModel()
	:desk(),
	nStep(0)
{
	tests();
}

GameModel::~GameModel()
{
}

void GameModel::start()
{
	bool playMore = true;
	std::string s;
	while (playMore) {
		reset();
		while (!setCommand()) {}
		drawGame();
		std::cout << "Game is finished, do you want to play one more time? y/n" << std::endl;
		std::getline(std::cin, s);
		playMore = s == "y";
	}
}

bool GameModel::setCommand()
{
	std::string s;
	std::getline(std::cin, s);
	return setCommand(s);
}

bool GameModel::setCommand(std::string s)
{
	CommandParser::Command c = parser.parseCommand(s);
	if (c.type != error) {
		return excecuteCommand(c);
	}
	else {
		std::cout << "Invalid command" << std::endl;
	}
	return false;
}

void GameModel::reset()
{
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			desk.cellsVector[i][j].isAlive = false;
		}
	}
	this->nStep = 0;
	this->historyStack = std::vector< std::vector< std::vector<Cell> > >();
	drawGame();
}

void GameModel::stepBack()
{
	if (historyStack.size() == 0) {
		drawGame();
		std::cout << "No history" << std::endl;
		return;
	}
	desk.cellsVector = historyStack.back();
	historyStack.pop_back();
	this->nStep--;
	drawGame();
}

void GameModel::setCell(std::string str)
{
	if (str.length() == 2) {
		int s = str[0] - 'A',
			f = 9 - (str[1] - '0');
		if (s < 10 && s >= 0 && f < 10 && f >= 0) {
			desk.cellsVector[f][s].isAlive = true;
			drawGame();
			return;
		}
	}
	drawGame();
	std::cout << "Wrong coordinates for setting cell" << std::endl;
}

bool GameModel::step(int n) //is game finished
{
	if (n > 0) {
		bool didAllDie = true;
		for (int k = 0; k < n; k++) {
			historyStack.push_back(desk.cellsVector);
			this->nStep++;
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					desk.cellsVector[i][j].checkState();
				}
			}
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 10; j++) {
					desk.cellsVector[i][j].applyState();
					if (desk.cellsVector[i][j].isAlive) {
						didAllDie = false;
					}
				}
			}
			if (didAllDie || isSameState(historyStack.back(), desk.cellsVector)) {
				return true;
			}
		}
		drawGame();
	}
	else {
		drawGame();
		std::cout << "Wrong step argument" << std::endl;
	}
	return false;
}

void GameModel::saveToFile(std::string fileName)
{
	std::ofstream fout;
	fout.open(fileName);
	if (fout.is_open()) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				fout << (desk.cellsVector[i][j].isAlive ? '*' : '.');
			}
		}
		drawGame();
		std::cout << "Saved into " << fileName << std::endl;
	}
	else {
		drawGame();
		std::cout << "Could not open file for writing" << std::endl;
	}

}

void GameModel::clearCell(std::string str)
{
	if (str.length() == 2) {
		int s = str[0] - 'A',
			f = 9 - (str[1] - '0');
		if (s < 10 && s >= 0 && f < 10 && f >= 0) {
			desk.cellsVector[f][s].isAlive = false;
			drawGame();
			return;
		}
	}
	drawGame();
	std::cout << "Wrong coordinates for clearing cell" << std::endl;
}

void GameModel::loadFromFile(std::string fileName) {
	std::ifstream fin;
	fin.open(fileName);
	char c;
	if (fin.is_open()) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				fin >> c;
				if (c == '.') {
					desk.cellsVector[i][j].isAlive = false;
				}
				else if (c == '*') {
					desk.cellsVector[i][j].isAlive = true;
				}
				else {
					reset();
					drawGame();
					std::cout << "Corrupted loading file. Unexpected symbol '" << c << "'" << std::endl;
					return;
				}
			}
		}
		this->nStep = 0;
		drawGame();
		std::cout << "Loaded from " << fileName << std::endl;
	}
	else {
		drawGame();
		std::cout << "Could not open file for reading" << std::endl;
	}
}

bool GameModel::excecuteCommand(CommandParser::Command c)
{
	switch (c.type) {
	case CommandType::reset:
		reset();
		break;
	case CommandType::set:
		setCell(c.strArg);
		break;
	case CommandType::step:
		return step(c.intArg);
		break;
	case CommandType::save:
		saveToFile(c.strArg);
		break;
	case CommandType::load:
		loadFromFile(c.strArg);
		break;
	case CommandType::clear:
		clearCell(c.strArg);
		break;
	case CommandType::back:
		stepBack();
		break;
	default:
		std::cout << "Unknown command" << std::endl;
	}
	return false;
}

bool GameModel::isSameState(std::vector<std::vector<Cell>> a, std::vector<std::vector<Cell>> b)
{
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			if (a[i][j].isAlive != b[i][j].isAlive) {
				return false;
			}
		}
	}
	return true;
}

void GameModel::drawGame()
{
	desk.draw();
	std::cout << "Step: " << nStep << std::endl;
}

void GameModel::tests()
{
	assert(parser.parseCommand("set A5").type == CommandType::set);
	assert(parser.parseCommand("set A").type == CommandType::set);
	assert(parser.parseCommand("set").type == CommandType::error);
	assert(parser.parseCommand("set Awefe23g43gg").type == CommandType::set);
	assert(parser.parseCommand("clear J6").type == CommandType::clear);
	assert(parser.parseCommand("clearJ6").type == CommandType::error);
	assert(parser.parseCommand("clear").type == CommandType::error);
	assert(parser.parseCommand("clear J6wegweg").type == CommandType::clear);
	assert(parser.parseCommand("step").type == CommandType::step);
	assert(parser.parseCommand("step 142").type == CommandType::step);
	assert(parser.parseCommand("step ").type == CommandType::error);
	assert(parser.parseCommand("stepppp").type == CommandType::error);
	assert(parser.parseCommand("load").type == CommandType::error);
	assert(parser.parseCommand("load adsd").type == CommandType::load);
	assert(parser.parseCommand("save").type == CommandType::error);
	assert(parser.parseCommand("save asefe").type == CommandType::save);
	assert(parser.parseCommand("back").type == CommandType::back);
	assert(parser.parseCommand("reset").type == CommandType::reset);

}

GameModel::CommandParser::CommandParser()
{
}

GameModel::CommandParser::~CommandParser()
{
}

GameModel::CommandParser::Command GameModel::CommandParser::parseCommand(std::string str)
{
	CommandParser::Command com;
	if (str == "back") {
		com.type = CommandType::back;
	}
	else if (str == "reset") {
		com.type = CommandType::reset;
	}
	else if (str == "step") {
		com.type = CommandType::step;
		com.intArg = 1;
	}
	else {
		std::string firstToken = str.substr(0, 4);
		if (firstToken == "set ") {
			std::string sub = str.substr(4, str.length() - 4);
			if (sub.size() <= 0) {
				com.type = error;
				return com;
			}
			com.type = CommandType::set;
			com.strArg = sub;
		}
		else {
			std::string firstToken = str.substr(0, 5);
			if (firstToken == "step ") {
				com.type = CommandType::step;
				std::string sub = str.substr(5, str.length() - 5);
				if (sub.size() <= 0) {
					com.type = error;
					return com;
				}
				int i = std::stoi(sub);
				com.intArg = i;
			}
			else if (firstToken == "save ") {
				com.type = CommandType::save;
				std::string sub = str.substr(5, str.length() - 5);
				if (sub.size() <= 0) {
					com.type = error;
					return com;
				}
				com.strArg = sub;
			}
			else if (firstToken == "load ") {
				com.type = CommandType::load;
				std::string sub = str.substr(5, str.length() - 5);
				if (sub.size() <= 0) {
					com.type = error;
					return com;
				}
				com.strArg = sub;
			}
			else {
				std::string firstToken = str.substr(0, 6);
				if (firstToken == "clear ") {
					std::string sub = str.substr(6, str.length() - 6);
					if (sub.size() <= 0) {
						com.type = error;
						return com;
					}
					com.type = CommandType::clear;
					com.strArg = sub;
				}
				else {
					com.type = CommandType::error;
				}
			}
		}
	}
	return com;
}
