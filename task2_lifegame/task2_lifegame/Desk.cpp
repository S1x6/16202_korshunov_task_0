#include "Desk.h"


Desk::Desk()
{
	cellsVector.resize(10);
	for (int i = 0; i < 10; i++) {
		cellsVector[i].resize(10);
	}
	std::vector<Cell*> vec(8);
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			vec.clear();
			vec.push_back(&cellsVector[(i + 1) % 10][j]);
			vec.push_back(&cellsVector[(i + 1) % 10][(j + 1) % 10]);
			vec.push_back(&cellsVector[i][(j + 1) % 10]);
			vec.push_back(&cellsVector[(i + 9) % 10][(j + 1) % 10]);
			vec.push_back(&cellsVector[(i + 1) % 10][(j + 9) % 10]);
			vec.push_back(&cellsVector[(i + 9) % 10][j]);
			vec.push_back(&cellsVector[i][(j + 9) % 10]);
			vec.push_back(&cellsVector[(i + 9) % 10][(j + 9) % 10]);
			cellsVector[i][j].setNeighbours(vec);
		}
	}
}

Desk::~Desk()
{
}

void Desk::draw()
{
	system("cls");
	std::cout << "  A B C D E F G H I J" << std::endl;
	for (int i = 0; i < 10; i++) {
		std::cout << 9 - i << " ";
		for (int j = 0; j < 10; j++) {
			if (cellsVector[i][j].isAlive) {
				std::cout << "* ";
			}
			else {
				std::cout << ". ";
			}
		}
		std::cout << std::endl;
	}
}