#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Desk.h"
#include "Cell.h"
class GameModel
{
public:
	GameModel();
	~GameModel();
	void start();

private:
	class CommandParser {
	public:
		struct Command {
			int type;
			std::string strArg;
			int intArg;
		};
		CommandParser();
		~CommandParser();
		Command parseCommand(std::string);
	};
	Desk desk;
	int nStep;
	CommandParser parser;
	std::vector< std::vector< std::vector<Cell> > > historyStack;
	bool setCommand(); // did game finish after command
	bool setCommand(std::string); // did game finish after command
	void reset();
	void stepBack();
	void clearCell(std::string);
	void loadFromFile(std::string fileName);
	void setCell(std::string);
	bool step(int n);
	void saveToFile(std::string fileName);
	bool excecuteCommand(CommandParser::Command c);
	bool isSameState(std::vector< std::vector<Cell> >, std::vector< std::vector<Cell> >);
	void drawGame();
	void tests();
};

