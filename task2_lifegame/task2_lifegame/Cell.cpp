#include "Cell.h"

Cell::Cell()
{
	isAlive = false;
	nextState = false;
}


Cell::~Cell()
{
}

void Cell::setNeighbours(std::vector<Cell*> neighbours)
{
	this->neighbours = neighbours;
}

void Cell::checkState()
{
	int aliveNeighbours = 0;
	for (int i = 0; i < 8; i++) {
		if (neighbours[i]->isAlive) {
			aliveNeighbours++;
		}
	}
	if (aliveNeighbours == 3) {
		this->nextState = true;
	}
	else if (aliveNeighbours > 3 || aliveNeighbours < 2) {
		this->nextState = false;
	}
	else {
		nextState = isAlive;
	}
}

void Cell::applyState()
{
	this->isAlive = this->nextState;
}
