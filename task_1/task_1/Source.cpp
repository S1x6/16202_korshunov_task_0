#include "src\gtest-all.cc"
#include "trit_set.h"


TEST(Simple, Simple) {
	ASSERT_EQ(5, 2 + 3);
}

int main(int argc, char** argv) {
	testing::InitGoogleTest(&argc, argv);
	trit::TritSet set(10);
	bool c = set[1] == trit::Unknown;
	set[1] = trit::False;
	c = set[1] == trit::Unknown;
	c = set[1] == trit::True;
	c = set[1] == trit::False;

	c = set[18] == trit::Unknown;
	c = set[18] == trit::True;
	c = set[18] == trit::False;
	
	set[18] = trit::True;
	c = set[18] == trit::Unknown;
	c = set[18] == trit::True;
	c = set[18] == trit::False;
	c = set[1] == trit::Unknown;
	c = set[1] == trit::True;
	c = set[1] == trit::False;
	return RUN_ALL_TESTS();
}