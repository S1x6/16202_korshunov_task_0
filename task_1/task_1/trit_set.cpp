#include "trit_set.h"

namespace trit {


	TritSet::TritSet() {
		size_trit = 16;
		size_elem = 1;
		arr = new unsigned int[1];
		memset(arr, 0, sizeof(int));
		iterator = Iterator(this);
	}

	TritSet::TritSet(int size) {
		size_elem = int(ceil(size / 4.0 / sizeof(unsigned int)));
		size_trit = size;
		arr = new unsigned int[this->size_elem];
		memset(arr, 0, sizeof(int)*size_elem);
		iterator = Iterator(this);
	}

	TritSet::~TritSet() {
		delete [] arr;
	}
	
	void TritSet::resize(int new_size) {
		int new_size_elem = int(ceil(new_size / 4.0 / sizeof(unsigned int)));
		unsigned int* new_arr = new unsigned int[new_size_elem];
		std::memset(new_arr, 0, new_size_elem * sizeof(unsigned int));
		std::memcpy(new_arr, arr, size_elem * sizeof(unsigned int));
		size_elem = new_size_elem;
		size_trit = new_size;
		delete [] arr;
		arr = new_arr;
	}
	int TritSet::capacity() {
		return this->size_elem;
	}

	int TritSet::size()
	{
		return size_trit;
	}

	Iterator TritSet::operator[](unsigned int n) {

		iterator.setState(n);
		return iterator;
	}


	/*TritSet operator&(TritSet& a, TritSet& b)
	{
		int max_size = a.capacity() > b.capacity ? a.capacity() : b.capacity();
		TritSet c(max_size);
		for (int i = 0; i < max_size; i++) {
			if (a[i] == False || b[i] == False) 
				c[i] = False; 
			else 
				if () 
		}
		return c;
	}*/

	Iterator::Iterator(TritSet* set) {
		integer = 0;
		cluster = 0;
		index = 0;
		this->set = set;
	}

	Trit Iterator::getValue() {
		int val = (set->arr[integer] >> (sizeof(unsigned int) * 8 - 2 - cluster*2)) & 3;
		if (val == True)
			return True;
		else if (val == False)
			return False;
		else return Unknown;
	}

	void Iterator::setState(int n)
	{
		index = n;
		integer = n / (4 * sizeof(unsigned int));
		cluster = n - (integer * sizeof(unsigned int)*4);
	}

	bool Iterator::operator==(Trit t)
	{
		return t == getValue();
	}

	void Iterator::operator=(Trit trit) {
		if (set->capacity() < integer+1) {
			if (trit == Unknown) {
					std::cout << sizeof(*set->arr);
					return;
				}
			else {
				set->resize(index);

			}
		}
		std::cout << sizeof(*set->arr);
		int max_trit_index_in_integer = sizeof(unsigned int) * 8 - 2;
		set->arr[integer] = (set->arr[integer] & (~(3 << (max_trit_index_in_integer - cluster)))) | (trit << (max_trit_index_in_integer - cluster*2));
	}
}