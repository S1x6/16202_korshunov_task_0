#pragma once
#include <cmath>
#include <cstring>
#include <iostream>

namespace trit {

	enum Trit {
		False = 1,
		Unknown = 0,
		True = 2,
	};

	class TritSet;

	class Iterator {
	private:
		TritSet* set;
		int index;
		int integer; //number of byte 0-(size_byte-1)
		int cluster; // number of two-bit cluster inside the byte 0-15
		Trit getValue();
	public:
		Iterator(TritSet*);
		void setState(int);
		bool operator==(Trit);
		void operator=(Trit);
	};

	class TritSet {
	private:
		unsigned int *arr;
		int size_elem;
		int size_trit;
		Iterator iterator = NULL;
	public:
		TritSet(int);
		TritSet();
		~TritSet();
		void resize(int);
		int capacity();
		int size();
		Iterator operator[](unsigned int);
		friend Iterator;
	};

	//TritSet operator&(TritSet& a, TritSet& b);
}