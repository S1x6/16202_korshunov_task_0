#include "WorkerSort.h"
#include <algorithm>



WorkerSort::WorkerSort()
{
}


WorkerSort::~WorkerSort()
{
}

Worker& WorkerSort::excecute()
{
	if (arguments.size() != 0)
	{
		throw std::invalid_argument("Wrong amount of input arguments in sort block with name \"" + name + "\"");
	}
	if (input.size() == 0) {
		throw std::runtime_error("Empty input for sort block with name \"" + name + "\"");
	}
	std::sort(input.begin(), input.end());
	output = input;
	return *this;
}
