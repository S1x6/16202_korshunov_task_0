#pragma once
#include <string>
#include "Worker.h"
#include <unordered_map>
#include <fstream>
#include <memory>
#include "WorkflowParser.h"

class Workflow
{
public:
	Workflow();
	Workflow(int argc, char **argv);
	Workflow(std::string fileName);
	~Workflow();
	void open(std::string fileName);
	void excecute();
private:
	std::string inputFile,
				outputFile;
	WorkflowParser parser;
	std::unordered_map<std::string, std::unique_ptr<Worker> > blocks;
	std::vector<std::string> scheme;
	std::ifstream fi;
};

