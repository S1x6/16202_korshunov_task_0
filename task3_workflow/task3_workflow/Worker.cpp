#include "Worker.h"



Worker::Worker()
{
}


Worker::~Worker()
{
}

Worker& Worker::pushArgument(std::string str)
{
	arguments.push_back(str);
	return *this;
}

Worker& Worker::setInput(std::vector<std::string>& v)
{
	input = v;
	return *this;
}

std::vector<std::string>& Worker::getOutput()
{
	return output;
}

void Worker::setName(std::string blockName)
{
	this->name = blockName;
}

std::string Worker::getName() const
{
	return name;
}
