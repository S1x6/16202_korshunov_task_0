#include "Workflow.h"
#include "WorkerReadFile.h"
#include "WorkerWriteFile.h"
#include <fstream>
#include <sstream>
#include <iostream>

Workflow::Workflow()
{
}

Workflow::Workflow(int argc, char ** argv)
{
	for (int i = 1; i < argc - 1; ++i) {
		if (std::string(argv[i]) == std::string("-i")) {
			inputFile = std::string(argv[i + 1]);
		}
 		else if (std::string(argv[i]) == std::string("-o")) {
			outputFile = std::string(argv[i + 1]);
		}
	}
}

Workflow::Workflow(std::string fileName)
{
	open(fileName);
}


Workflow::~Workflow()
{
}

void Workflow::open(std::string fileName)
{
	fi = std::ifstream(fileName);
	if (!fi.is_open()) {
		throw std::invalid_argument("No such file \"" + fileName + "\"");
	}
}

void Workflow::excecute()
{
	if (!fi.is_open()) {
		throw std::runtime_error("File stream was not opened for excecution");
	}
	blocks = parser.parseBlocks(fi);
	scheme = parser.parseCommands(fi);
	if (blocks.size() == 0) { return; }
	if (!parser.hasInput()) {
		if (inputFile != "")
			blocks[scheme[0]]->setInput(WorkerReadFile().pushArgument(inputFile).excecute().getOutput());
		else
			throw std::runtime_error("Missing readfile command");
	}
	
	blocks[scheme[0]]->excecute();
	for (size_t i = 1; i < scheme.size(); ++i) {
		blocks[scheme[i]]->setInput(blocks[scheme[i - 1]]->getOutput());
		blocks[scheme[i]]->excecute();
	}
	if (!parser.hasOutput()) {
		if (outputFile != "")
			WorkerWriteFile().pushArgument(outputFile).setInput(blocks[scheme.back()]->getOutput()).excecute().getOutput();
		else 
			throw std::runtime_error("Missing writefile command");
	}
}