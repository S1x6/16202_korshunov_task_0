#pragma once
#include <vector>
#include <string>
class Worker
{
public:
	Worker();
	~Worker();
	virtual Worker& excecute() = 0;
	Worker& pushArgument(std::string);
	Worker& setInput(std::vector<std::string>&);
	std::vector<std::string>& getOutput();
	void setName(std::string);
	std::string getName() const;
protected:
	std::string name;
	std::vector< std::string > arguments;
	std::vector< std::string > input;
	std::vector< std::string > output;
	
};

