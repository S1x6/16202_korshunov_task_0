#include "WorkerGrep.h"



WorkerGrep::WorkerGrep()
{
}


WorkerGrep::~WorkerGrep()
{
}

Worker& WorkerGrep::excecute()
{
	if (arguments.size() != 1)
	{
		throw std::invalid_argument("Wrong amount of input arguments in grep block with name \"" + name + "\"");
	}
	if (input.size() == 0) {
		throw std::runtime_error("Empty input for grep block with name \"" + name + "\"");
	}
	for (size_t i = 0; i < input.size(); ++i) {
		if (input[i].find(arguments[0], 0) != std::string::npos) {
			output.push_back(input[i]);
		}
	}
	return *this;
}
