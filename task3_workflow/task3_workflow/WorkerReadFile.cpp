#include "WorkerReadFile.h"
#include <exception>
#include <fstream>

WorkerReadFile::WorkerReadFile()
{
}


WorkerReadFile::~WorkerReadFile()
{
}

Worker& WorkerReadFile::excecute() {
	if (arguments.size() != 1)
	{
		throw std::invalid_argument("Wrong amount of input arguments in read file block with name \"" + name + "\"");
	}
	if (input.size() != 0) {
		throw std::runtime_error("Not empty input while reading file in block with name \""+name+"\". Perhaps this command is not the first one in excecute queue");
	}
	std::ifstream fi = std::ifstream(arguments[0]);
	if (!fi.is_open()) {
		throw std::invalid_argument("Could not open file \"" + arguments[0] + "\" in the block with name \"" + name + "\"");
	}
	std::string str;
	output.clear();
	while (!fi.eof()) {
		std::getline(fi, str);
		output.push_back(str);
	}
	return *this;
}
