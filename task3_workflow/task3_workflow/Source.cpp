#define _CRT_SECURE_NO_WARNINGS
#include "Workflow.h"
#include <iostream>

int main(int argc, char **argv) {
	if (argc > 1) {
		Workflow flow(argc, argv);
		try {
			flow.open(argv[1]);
			flow.excecute();
		}
		catch (const std::invalid_argument& e) {
			std::cout << "An error occured. Message: " << e.what();
			return 1;
		}
		catch (const std::runtime_error& e) {
			std::cout << "An error occured. Message: " << e.what();
			return 1;
		}
	}
	return 0;
}