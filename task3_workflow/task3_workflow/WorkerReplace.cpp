#include "WorkerReplace.h"
#include <iostream>

std::string replaceAll(std::string& str, const std::string& from, const std::string& to);

WorkerReplace::WorkerReplace()
{
}


WorkerReplace::~WorkerReplace()
{
}

Worker& WorkerReplace::excecute()
{
	if (arguments.size() != 2)
	{
		throw std::invalid_argument("Wrong amount of input arguments in replace block with name \"" + name + "\"");
	}
	if (input.size() == 0) {
		throw std::runtime_error("Empty input for replace block with name \"" + name + "\"");
	}
	for (size_t i = 0; i < input.size(); ++i) {
		output.push_back(replaceAll(input[i], arguments[0], arguments[1]));
	}
	return *this;
}

std::string WorkerReplace::replaceAll(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		size_t end_pos = start_pos + from.length();
		str.replace(start_pos, end_pos, to);
		start_pos += to.length();
	}
	return str;
}