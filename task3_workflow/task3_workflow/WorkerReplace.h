#pragma once
#include "Worker.h"
class WorkerReplace :
	public Worker
{
public:
	WorkerReplace();
	~WorkerReplace();
	Worker& excecute();
private:
	std::string replaceAll(std::string& str, const std::string& from, const std::string& to);
};

