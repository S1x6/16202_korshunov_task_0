#pragma once
#include <unordered_map>
#include "Worker.h"
#include <memory>
#include <fstream>


class WorkflowParser
{
public:
	WorkflowParser();
	~WorkflowParser();
	std::unordered_map<std::string, std::unique_ptr<Worker>> parseBlocks(std::ifstream &fi);
	std::vector<std::string> parseCommands(std::ifstream &fi);
	bool hasInput();
	bool hasOutput();
private:
	bool _hasInput;
	bool _hasOutput;
	void pushWorker(std::string& str, std::unordered_map<std::string, std::unique_ptr<Worker>>& blocks);
	std::vector<std::string> split(std::string str, char c);
};

