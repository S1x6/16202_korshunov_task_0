#include "WorkerDump.h"
#include <fstream>
#include <string>


WorkerDump::WorkerDump()
{
}


WorkerDump::~WorkerDump()
{
}

Worker& WorkerDump::excecute()
{
	if (arguments.size() != 1)
	{
		throw std::invalid_argument("Wrong amount of input arguments in dump block with name \"" + name + "\"");
	}
	std::ofstream fo = std::ofstream(arguments[0]);
	std::string str;
	output.clear();
	for (size_t i = 0; i < input.size(); ++i) {
		fo << input[i];
		fo << std::endl;
	}
	output = input;
	return *this;
}