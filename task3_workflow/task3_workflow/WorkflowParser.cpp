#include "WorkflowParser.h"
#include "WorkerReadFile.h"
#include "WorkerWriteFile.h"
#include "WorkerDump.h"
#include "WorkerSort.h"
#include "WorkerReplace.h"
#include "WorkerGrep.h"
#include <sstream>

std::vector<std::string> WorkflowParser::parseCommands(std::ifstream & fi)
{
	std::vector<std::string> scheme;
	std::string str;
	std::getline(fi, str);
	size_t pos = 0;
	std::string token;
	while ((pos = str.find(" -> ")) != std::string::npos) {
		token = str.substr(0, pos);
		if (token.find("->") == std::string::npos && token != "") {
			scheme.push_back(token);
			str.erase(0, pos + 4);
		}
		else {
			throw std::runtime_error("Wrong format, command \"" + token + "\" was not parsed.");
		}
	}
	if (str.find("->") == std::string::npos && str != "") {
		scheme.push_back(str);
	}
	else {
		throw std::runtime_error("Wrong format, command \"" + str + "\" was not parsed.");
	}
	return scheme;
}

bool WorkflowParser::hasInput()
{
	return _hasInput;
}

bool WorkflowParser::hasOutput()
{
	return _hasOutput;
}

WorkflowParser::WorkflowParser()
	: _hasInput(false),
	_hasOutput(false)
{
}

WorkflowParser::~WorkflowParser()
{
}

std::unordered_map<std::string, std::unique_ptr<Worker>> WorkflowParser::parseBlocks(std::ifstream &fi)
{
	std::unordered_map<std::string, std::unique_ptr<Worker>> blocks;
	std::string str;
	std::getline(fi, str);
	if (str == "desc") {
		while (!fi.eof()) {
			std::getline(fi, str);
			if (str == "csed") break;
			pushWorker(str, blocks);
		}
	}
	else { throw std::runtime_error("Could not parse file. \"desc\" was expected but \""+str+"\" found"); }
	return blocks;
}

void WorkflowParser::pushWorker(std::string& str, std::unordered_map<std::string, std::unique_ptr<Worker>>& blocks) {
	std::string name;
	std::vector<std::string> v_name = split(str, '=');
	if (v_name.size() < 2) { throw std::runtime_error("Block was not parsed. Expected \"[block_name] = [command] {argument}...\", but \""+str+"\" found"); }
	name = v_name[0].substr(0, v_name[0].size() - 1);
	std::vector<std::string> v_comm = split(v_name[1].substr(1, v_name[1].size() - 1), ' ');
	Worker *w;
	if (v_comm[0] == "readfile") {
		w = new WorkerReadFile();
		_hasInput = true;
	}
	else if (v_comm[0] == "writefile") {
		w = new WorkerWriteFile();
		_hasOutput = true;
	}
	else if (v_comm[0] == "dump") {
		w = new WorkerDump();
	}
	else if (v_comm[0] == "replace") {
		w = new WorkerReplace();
	}
	else if (v_comm[0] == "grep") {
		w = new WorkerGrep();
	}
	else if (v_comm[0] == "sort") {
		w = new WorkerSort();
	}
	else {
		throw std::runtime_error("Unknown command. Expected \"readfile\", \"writefile\", \"dump\", \"replace\", \"grep\", \"sort\" but \"" + v_comm[0] + "\" found");
	}

	for (size_t i = 1; i < v_comm.size(); ++i) {
		w->pushArgument(v_comm[i]);
	}
	w->setName(name);
	blocks[name] = std::unique_ptr<Worker>(w);
}

std::vector<std::string> WorkflowParser::split(std::string str, char c) {
	std::stringstream s(str);
	std::string ss;
	std::vector<std::string> v;
	while (std::getline(s, ss, c)) {
		v.push_back(ss);
	}
	return v;
}