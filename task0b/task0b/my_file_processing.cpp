#include "my_file_processing.h"

namespace MyFileProcessing
{
	std::list<std::string>* readListFromFile(std::string file_name) {
		std::list<std::string> *list = new std::list<std::string>();
		std::ifstream fin;
		fin.open(file_name, std::ifstream::in);
		if (!fin.is_open()) {
			return NULL;
		}
		std::string str;
		while (getline(fin, str)) {
			list->push_back(str);
		}
		return list;
	}

	void writeListToFile(std::string file_name, std::list<std::string> *list) {
		std::ofstream fout;
		fout.open(file_name, std::ifstream::out);
		if (fout) {
			while (list->size() != 0) {
				std::string str = list->front();
				list->pop_front();
				fout << str << "\n";
			}
		}
	}
}