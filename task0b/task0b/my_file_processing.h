#pragma once
#include <string>
#include <list>
#include <fstream>
#include <iostream>

namespace MyFileProcessing
{
	std::list<std::string>* readListFromFile(std::string file_name);
	void writeListToFile(std::string file_name, std::list<std::string> *list);
}