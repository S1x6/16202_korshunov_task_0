#pragma once
#include <string>
#include <list>

namespace MyStringProcessing
{
	void sort_strings(std::list<std::string>* list);
}