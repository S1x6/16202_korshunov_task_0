#include "my_error_handler.h"

namespace MyErrorHandler {
	int finishProgram(int error_code)
	{
		switch (error_code)
		{
		case WRONG_KEYS_NUMBER:
			std::cout << "Wrong number of input keys\n";
			break;
		case WRONG_INPUT_NAME:
			std::cout << "Input file does no exist or cannot be opened\n";
			break;
		default:
			break;
		}
		return error_code;
	}
}
