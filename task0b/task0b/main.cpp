#include "my_file_processing.h"
#include "my_string_processing.h"
#include "my_error_handler.h"

int main(int argc, char** argv) {
	if (argc == 3) {
		std::list<std::string> *list = MyFileProcessing::readListFromFile(std::string(argv[1]));
		if (list == NULL) return MyErrorHandler::finishProgram(MyErrorHandler::WRONG_INPUT_NAME);
		MyStringProcessing::sort_strings(list);
		MyFileProcessing::writeListToFile(argv[2], list);
		delete list;
		return 0;
	}
	else {
		return MyErrorHandler::finishProgram(MyErrorHandler::WRONG_KEYS_NUMBER);
	}
}