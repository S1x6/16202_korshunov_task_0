#pragma once
#include <iostream>

namespace MyErrorHandler {
	const int WRONG_KEYS_NUMBER = -1;
	const int WRONG_INPUT_NAME = -2;

	int finishProgram(int error_code);
}